''' controller and routes for user settings '''
import os
from flask import request
from src.common.common import answer
from src.schemas.settings import validate_budget, validate_checking_account
from src.log import log as logger
from src import app, mongo
from flask_jwt_extended import (jwt_required,
                                get_jwt_identity)


ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))


@app.route('/data/set_budget', methods=['POST'])
@jwt_required
def configure_budget():
    current_user = get_jwt_identity()
    bad_request_parameters = answer(ok=True,
                                    message='Bad request parameters!',
                                    error_code=400)

    input_data = validate_budget(request.get_json())
    if not(input_data['ok']):
        message = 'Bad request parameters: {}'.format(input_data['message'])
        return answer(ok=False, message=message, error_code=400)

    data = {'$set': {'budget_configuration': input_data['data']}}
    query = {'email': current_user['email']}
    try:
        LOG.info(f'inserting data {input_data}')
        mongo.db.settings.update_one(query, data, upsert=True)
    except:
        return bad_request_parameters

    return answer(ok=True, message='success', error_code=200)


@app.route('/data/get_budget', methods=['GET'])
@jwt_required
def get_budget():
    current_user = get_jwt_identity()
    raw_data = request.get_json()
    data = mongo.db.settings.find_one({'email': current_user['email']})
    LOG.info(f'read {data} for user: {current_user["email"]}')
    response = data.get('budget_configuration', {}) if data else {}
    return answer(ok=True, data=response, error_code=200)


@app.route('/data/set_checking_account', methods=['POST'])
@jwt_required
def configure_checking_account():
    current_user = get_jwt_identity()
    bad_request_parameters = answer(ok=True,
                                    message='Bad request parameters!',
                                    error_code=400)

    input_data = validate_checking_account(request.get_json())
    if not(input_data['ok']):
        message = 'Bad request parameters: {}'.format(input_data['message'])
        return answer(ok=False, message=message, error_code=400)

    data = {'$set': {'checking_account': input_data['data']}}
    query = {'email': current_user['email']}
    try:
        LOG.info(f'inserting data {input_data}')
        mongo.db.settings.update_one(query, data, upsert=True)
    except:
        return bad_request_parameters

    return answer(ok=True, message='success', error_code=200)

@app.route('/data/get_checking_account', methods=['GET'])
@jwt_required
def get_checking_account():
    current_user = get_jwt_identity()
    raw_data = request.get_json()
    data = mongo.db.settings.find_one({'email': current_user['email']})
    LOG.info(f'read {data} for user: {current_user["email"]}')
    response = data.get('checking_account', {}) if data else {}
    return answer(ok=True, data=response, error_code=200)
