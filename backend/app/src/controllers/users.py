''' controller and routes for users '''
import os
from flask import request, jsonify
from src.common.common import answer
from src.schemas.users import validate_user, validate_google_user
from src.log import log as logger
from src import app, mongo, flask_bcrypt, jwt
from flask_jwt_extended import (create_access_token,
                                create_refresh_token,
                                jwt_required,
                                jwt_refresh_token_required,
                                get_jwt_identity)
from google.oauth2 import id_token
from google.auth.transport import requests

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))


@app.route('/data/register', methods=['POST'])
def register():
    ''' register user endpoint '''
    LOG.info("register new user")

    data = validate_user(request.get_json())
    if data['ok']:
        data = data['data']
        data['password'] = flask_bcrypt.generate_password_hash(
                            data['password'])
        mongo.db.users.insert_one(data)
        message = 'User created succesfully!'
        ok = True
        error_code = 200
    else:
        message = 'Bad request parameters: {}'.format(data['message'])
        ok = False
        error_code = 400

    return answer(ok=ok, message=message, error_code=error_code)


@app.route('/data/auth', methods=['POST'])
def auth_user():
    ''' auth endpoint '''
    LOG.info('authenticate user')

    data = validate_user(request.get_json())
    if data['ok']:
        data = data['data']
        user = mongo.db.users.find_one({'email': data['email']}, {"_id": 0})
        if user and flask_bcrypt.check_password_hash(user['password'], data['password']):
            del user['password']
            access_token = create_access_token(identity=data)
            refresh_token = create_refresh_token(identity=data)
            user['token'] = access_token
            user['refresh'] = refresh_token
            return answer(ok=True, data=user, error_code=200)
        else:
            return answer(ok=False, message='invalid username or password', error_code=401)

    else:
        message = 'Bad request parameters: {}'.format(data['message'])
        return answer(ok=False, message=message, error_code=400)

CLIENT_ID='464194612753-0dgbfa559vhhapv9r4t705irceva72tv.apps.googleusercontent.com'
@app.route('/data/auth-google', methods=['POST'])
def auth_google_user():
    ''' auth google endpoint '''
    LOG.info('authenticate google user')

    data = validate_google_user(request.get_json())
    if data['ok']:
        token = data['data']['token']
        idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
        # Or, if multiple clients access the backend server:
        # idinfo = id_token.verify_oauth2_token(token, requests.Request())
        # if idinfo['aud'] not in [CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]:
        #     raise ValueError('Could not verify audience.')

        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            return answer(ok=False, message='invalid username or password', error_code=401)
        # If auth request is from a G Suite domain:
        # if idinfo['hd'] != GSUITE_DOMAIN_NAME:
        #     raise ValueError('Wrong hosted domain.')

        # ID token is valid. Get the user's Google Account ID from the decoded token.
        userid = idinfo['sub']
        LOG.info(f'user is is {userid}')
        access_token = create_access_token(identity={'email':userid})
        refresh_token = create_refresh_token(identity={'email':userid})
        user_return = {'token': access_token, 'refresh': refresh_token}
        return answer(ok=True, data=user_return, error_code=200)

    else:
        message = 'Bad request parameters: {}'.format(data['message'])
        return answer(ok=False, message=message, error_code=400)


@app.route('/data/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    ''' refresh token endpoint '''
    current_user = get_jwt_identity()
    LOG.debug('refresh token for user {}'.format(current_user))

    ret = {
            'token': create_access_token(identity=current_user)
    }
    return answer(ok=True, data=ret, error_code=200)


@jwt.unauthorized_loader
def unauthorized_response(callback):
    return answer(ok=False, message='Missing Authorization Header', error_code=401)
