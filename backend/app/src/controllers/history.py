''' controller and routes for users '''
import os
from flask import request
from src.schemas.transactions import validate_transaction
from src.log import log as logger
from src import app, mongo
from flask_jwt_extended import (jwt_required,
                                get_jwt_identity)
from src.common.common import answer
from src.common.common_history import perform_transaction, record_transaction
ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))


def validate_asset(user, transaction, name):
    if name in transaction.keys():
        query = {'email': user['email'], 'asset': transaction[name]}
        if mongo.db.assets.count(query) == 0:
            message = 'Bad {} asset: {}'.format(name, transaction[name])
            return answer(ok=False, message=message, error_code=400)
    return None


@app.route('/data/transaction', methods=['POST'])
@jwt_required
def set_transaction():
    current_user = get_jwt_identity()

    data = validate_transaction(request.get_json())
    if not(data['ok']):
        message = 'Bad request parameters: {}'.format(data['message'])
        return answer(ok=False, message=message, error_code=400)

    transaction = data['data']

    result = validate_asset(current_user, transaction, 'source')
    if (result is not None):
        return result

    result = validate_asset(current_user, transaction, 'target')
    if result is not None:
        return result

    perform_transaction(current_user['email'], transaction, mongo.db.assets)
    record_transaction(current_user['email'], transaction, mongo.db.history)
    return answer(ok=True, data=data, error_code=200)


@app.route('/data/history', methods=['GET'])
@jwt_required
def get_history():
    current_user = get_jwt_identity()
    asset = request.args.get('asset', '')
    query = {'email': current_user['email'],
             '$or':[{'source': asset}, {'target': asset}]}

    data = []
    for entry in mongo.db.history.find(query):
        new_entry = entry.copy()
        new_entry.pop('email')
        new_entry.pop('_id')
        data.append(entry)

    return answer(ok=True, data=data, error_code=200)


@app.route('/data/periodic', methods=['GET'])
@jwt_required
def get_periodic():
    current_user = get_jwt_identity()
    query = {'email': current_user['email']}
    data = []
    for entry in mongo.db.periodic.find(query):
        new_entry = entry.copy()
        new_entry.pop('email')
        new_entry.pop('_id')
        data.append(entry)

    return answer(ok=True, data=data, error_code=200)



@app.route('/data/periodic', methods=['POST'])
@jwt_required
def set_periodic():
    current_user = get_jwt_identity()
    query = {'email': current_user['email']}
    data = request.get_json()
    LOG.warning("this is data " + str(data))
    data['email'] = current_user['email']
    data['left'] = data['periods']
    mongo.db.periodic.insert_one(data)
    return answer(ok=True, data=data, error_code=200)
