from mock import Mock
from src.schemas.transactions import validate_transaction
from src.controllers.history import (perform_transaction,
                                     record_transaction,
                                     validate_asset)


def withdraw_request(asset, amount, tag):
    return {'name': 'WITHDRAW',
            'source': asset,
            'amount': amount,
            'tag': tag}


def deposit_request(asset, amount, tag):
    return {'name': 'DEPOSIT',
            'target': asset,
            'amount': amount,
            'tag': tag}


def transfer_request(source, to, amount, tag):
    return {'name': 'TRANSFER',
            'source': source,
            'target': to,
            'amount': amount,
            'tag': tag}

def bad_deposit_request(asset, amount, tag):
    return {'name': 'DEPOSIT',
            'source': asset,
            'amount': amount,
            'tag': tag}

def test_validate():
    data = validate_transaction(withdraw_request('myasset', 1000, 'this is tag'))
    assert data["ok"]
    data = validate_transaction(deposit_request('myasset', 1000, 'this is tag'))
    assert data["ok"]
    data = validate_transaction(transfer_request('myasset',
                                            'second_asset',
                                            1000,
                                            'this is tag'))
    assert data["ok"]
    data = validate_transaction(bad_deposit_request('myasset', 1000, 'this is tag'))
    assert not(data["ok"])


def test_withdraw():
    asset = 'test_amount'
    original_amount = 4000
    transaction_amount = 100
    transaction_user = 'test@mail.com'
    id = 'whatever'
    assets_mock = Mock()
    assets_mock.find_one.return_value = {'email': transaction_user,
                                          'asset': asset,
                                          'amount': original_amount,
                                          '_id': id}
    perform_transaction(transaction_user,
                        withdraw_request(asset, transaction_amount, "tag"),
                        assets_mock)
    assets_mock.update_one.assert_called_once_with({"_id": id},
                         {"$set": {"amount": original_amount - transaction_amount}},
                         upsert=False)


def test_deposit():
    asset = 'test_amount'
    original_amount = 4000
    transaction_amount = 100
    transaction_user = 'test@mail.com'
    id = 'whatever'
    assets_mock = Mock()
    assets_mock.find_one.return_value = {'email': transaction_user,
                                          'asset': asset,
                                          'amount': original_amount,
                                          '_id': id}
    perform_transaction(transaction_user,
                        deposit_request(asset, transaction_amount, "tag"),
                        assets_mock)
    assets_mock.update_one.assert_called_once_with({"_id": id},
                         {"$set": {"amount": original_amount + transaction_amount}},
                         upsert=False)


#TODO: add test_transfer ( abit hard because of side effect mocking)
