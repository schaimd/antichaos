''' controller and routes for users '''
import os
from datetime import datetime, timedelta
from flask import request
from src.common.common import answer
from src.schemas.expenses import validate_add_expense
from src.log import log as logger
from src import app, mongo
from flask_jwt_extended import (jwt_required,
                                get_jwt_identity)


ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))

def get_dates(date):
    start_date = datetime(month=date.month, year=date.year, day=1)
    end_date = start_date + timedelta(days=31)
    end_date = datetime(month=end_date.month, year=end_date.year, day=1)
    return start_date, end_date

@app.route('/data/expenses', methods=['GET'])
@jwt_required
def get_expenses():
    current_user = get_jwt_identity()
    raw_data = request.get_json()
    query = {}
    date = datetime.fromtimestamp(request.args.get('date', 0, type=int))
    start_date, end_date = get_dates(date)
    LOG.info("looking for expenses between the dates {} - {}".format(start_date,
        end_date))
    query['date'] = {"$gt": start_date.timestamp(), "$lt": end_date.timestamp()}
    query['email'] = current_user['email']
    data = {}
    total = 0
    data['expenses'] = []
    for entry in mongo.db.expenses.find(query).sort('date', -1):
        data['expenses'].append(entry)
        total += entry['amount']
    data['total'] = total

    return answer(ok=True, data=data, error_code=200)

@app.route('/data/monthly_expenses', methods=['GET'])
@jwt_required
def get_mothly_expenses():
    current_user = get_jwt_identity()
    raw_data = request.get_json()
    query = {}
    year = request.args.get('year', 0, type=int)
    start_date = datetime(year=year, month=1, day=1)
    LOG.info(f'looking for expenses later than {start_date}.')
    query['date'] = {'$gt': start_date.timestamp()}
    query['email'] = current_user['email']
    data = {}
    for entry in mongo.db.expenses.find(query):
        month = datetime.fromtimestamp(entry['date']).month
        category = entry['category']
        data[month] = data.get(month, {})
        data[month][category] = data[month].get(category, 0) + entry['amount']

    return answer(ok=True, data=data, error_code=200)

@app.route('/data/expense', methods=['POST'])
@jwt_required
def add_expense():
    current_user = get_jwt_identity()
    bad_request_parameters = answer(ok=True,
                                    message='Bad request parameters!',
                                    error_code=400)

    data = validate_add_expense(request.get_json())
    if not(data['ok']):
        message = 'Bad request parameters: {}'.format(data['message'])
        return answer(ok=False, message=message, error_code=400)

    data = data['data']
    data['email'] = current_user['email']
    try:
        LOG.info("inserting data {}".format(data))
        mongo.db.expenses.insert_one(data)
    except:
        return bad_request_parameters

    return answer(ok=True, message='success', error_code=200)


@app.route('/data/expense', methods=['DELETE'])
@jwt_required
def remove_expense():
    current_user = get_jwt_identity()
    LOG.warning("expense command for user {}".format(current_user))
    bad_request_parameters = answer(ok=True,
                                    message='Bad request parameters!',
                                    error_code=400)

    data = validate_add_expense(request.get_json())
    if not(data['ok']):
        message = 'Bad request parameters: {}'.format(data['message'])
        return answer(ok=False, message=message, error_code=400)

    data = data['data']
    data['email'] = current_user['email']
    try:
        LOG.info("removing data {}".format(data))
        mongo.db.expenses.remove(data)
    except:
        return bad_request_parameters

    return answer(ok=True, message='success', error_code=200)
