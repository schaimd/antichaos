import investpy
import os
import json
from datetime import datetime, timedelta
from flask import request
from src.common.common import answer
from src.log import log as logger
from src import app, mongo
from src.schemas.market import validate_shares

ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))

SHARE_TYPES = (('etf', investpy.search_etfs, investpy.get_etf_recent_data),
               ('bond', investpy.search_bonds, investpy.get_bond_recent_data),
               )

COMMON_ARGS = {
    'as_json': True,
    'country': 'israel',
    'stock_exchange': 'Tel Aviv'
}

def get_share_price(query_func, share_args):
    results = json.loads(query_func(**share_args))
    return results['recent'][-1]['close']

def get_symbol_price(symbol):
    for share_type, type_func, query_func in SHARE_TYPES:
        try:
            result = type_func(by='symbol', value=symbol)
            name = json.loads(result.name.to_json())['0']
            args = COMMON_ARGS.copy()
            args[share_type]= name
            return get_share_price(query_func, args)
        except RuntimeError:
            LOG.info(f'symbol {symbol} is not {share_type}')

def get_new_prices(symbols):
    date = datetime.today()
    ret = {}
    for symbol_content in symbols:
        LOG.info(f'getting {symbol_content}')
        symbol = symbol_content
        if isinstance(symbol_content, tuple):
            symbol, id = symbol_content
        # price is in ILS cents, so we convert it to ILS.
        value = get_symbol_price(symbol) / 100
        inserted = {'symbol': symbol, 'value': value, 'date': date}
        if isinstance(symbol_content, tuple):
            inserted['_id'] = id
            mongo.db.market.update({'_id': id},inserted, True)
        else:
            mongo.db.market.insert(inserted)
        ret[symbol] = value
    return ret

@app.route('/data/market', methods=['POST'])
def get_market_price():
    data = validate_shares(request.get_json())
    if not(data['ok']):
        message = 'Bad request parameters: {}'.format(data['message'])
        return answer(ok=False, message=message, error_code=400)
    data = data['data']
    response = {}
    recalculate = set(data['symbols'])
    LOG.info(f'recalculating {recalculate}')
    for entry in mongo.db.market.find({'symbol': {'$in':data['symbols']}}):
        recalculate.remove(entry['symbol'])
        if entry['date'].date() < datetime.today().date():
            recalculate.add((entry['symbol'], entry['_id']))
        else:
            response[entry['symbol']] = entry['value']
    updated = get_new_prices(list(recalculate))
    response.update(updated)
    return answer(ok=True, data=response, error_code=200)
