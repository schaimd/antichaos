''' controller and routes for users '''
import os
from flask import request
from src.common.common import answer
from src.schemas.assets import validate_assets
from src.log import log as logger
from src import app, mongo
from flask_jwt_extended import (jwt_required,
                                get_jwt_identity)


ROOT_PATH = os.environ.get('ROOT_PATH')
LOG = logger.get_root_logger(
    __name__, filename=os.path.join(ROOT_PATH, 'output.log'))


@app.route('/data/assets', methods=['GET'])
@jwt_required
def get_assets():
    current_user = get_jwt_identity()
    raw_data = request.get_json()
    if raw_data is None:
        query = {}
    else:
        data = validate_assets(raw_data)
        if not(data['ok']):
            message = 'Bad request parameters: {}'.format(data['message'])
            return answer(ok=False, message=message, error_code=400)

        query = data['data']

    query['email'] = current_user['email']
    data = {}
    for entry in mongo.db.assets.find(query):
        data[entry['asset']] = entry['amount']

    return answer(ok=True, data=data, error_code=200)


@app.route('/data/assets', methods=['POST'])
@jwt_required
def add_asset():
    ''' route read user assets '''
    current_user = get_jwt_identity()
    LOG.warning("asset command for user {}".format(current_user))
    bad_request_parameters = answer(ok=True,
                                    message='Bad request parameters!',
                                    error_code=400)

    data = validate_assets(request.get_json())
    if not(data['ok']):
        message = 'Bad request parameters: {}'.format(data['message'])
        return answer(ok=False, message=message, error_code=400)

    data = data['data']
    data['email'] = current_user['email']
    try:
        mongo.db.assets.insert_one(data)
    except:
        return bad_request_parameters

    return answer(ok=True, message='success', error_code=200)


@app.route('/data/assets', methods=['DELETE'])
@jwt_required
def remove_asset():
    ''' route delete user assets '''
    current_user = get_jwt_identity()
    LOG.warning("asset command for user {}".format(current_user))
    bad_request_parameters = answer(ok=True,
                                    message='Bad request parameters!',
                                    error_code=400)

    data = validate_assets(request.get_json())
    if not(data['ok']):
        message = 'Bad request parameters: {}'.format(data['message'])
        return answer(ok=False, message=message, error_code=400)

    data = data['data']
    data['email'] = current_user['email']
    try:
        mongo.db.assets.remove(data)
    except:
        return bad_request_parameters

    return answer(ok=True, message='success', error_code=200)
