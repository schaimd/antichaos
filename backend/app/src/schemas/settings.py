from jsonschema import validate
from jsonschema.exceptions import ValidationError, SchemaError

budget_schema = {
    "type": "object",
    "properties": {
        "income": {
            "type": "number"
        },
        "budget": {
            "type": "number"
        },
    },
    "additionalProperties": False
}

def validate_budget(data):
    try:
        validate(data, budget_schema)
    except (ValidationError, SchemaError) as e:
        return {'ok': False, 'message': e}

    return {'ok': True, 'data': data}

checking_schema = {
    "type": "object",
    "properties": {
        "asset": {
            "type": "string"
        },
        "date": {
            "type": "number"
        }
    },
    "additionalProperties": False
}

def validate_checking_account(data):
    try:
        validate(data, checking_schema)
    except (ValidationError, SchemaError) as e:
        return {'ok': False, 'message': e}

    return {'ok': True, 'data': data}
