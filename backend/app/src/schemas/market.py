from jsonschema import validate
from jsonschema.exceptions import ValidationError, SchemaError

market_schema = {
    "type": "object",
    "properties": {
        "symbols": {
          "type": "array",
          "items": {
            "type": "string"
          },
          "default": []
        }
    },
    "additionalProperties": False
}



def validate_shares(data):
    try:
        validate(data, market_schema)
    except (ValidationError, SchemaError) as e:
        return {'ok': False, 'message': e}

    return {'ok': True, 'data': data}
