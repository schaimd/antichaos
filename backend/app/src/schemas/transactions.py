from jsonschema import validate
from jsonschema.exceptions import ValidationError, SchemaError


transaction_schema = {
    "type": "object",
    "properties": {
          "name": {
              "type": "string",
              "enum": ["WITHDRAW", "DEPOSIT", "TRANSFER"]
          },
            "amount": {
                "type": "number"
            },
            "tag": {
                "type": "string"
            },
            "source": {
                "type": "string"
            },
            "target": {
                "type": "string"
            },
    },
    "required": ["name", "amount", "tag"],
    "additionalProperties": False,
    "anyOf": [
        {
            "properties": {
                "name": { "enum" : ["WITHDRAW"]}
            },
            "required": ["source"]
        },
        {
            "properties": {
                "name": { "enum" : ["DEPOSIT"]}
            },
            "required": ["target"]
        },
        {
            "properties": {
                "name": { "enum" : ["TRANSFER"]}
            },
            "required": ["source", "target"]
        },
    ]
}


def validate_transaction(data):
    try:
        validate(data, transaction_schema)
    except (ValidationError, SchemaError) as e:
        return {'ok': False, 'message': e}

    return {'ok': True, 'data': data}
