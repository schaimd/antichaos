from jsonschema import validate
from jsonschema.exceptions import ValidationError, SchemaError

add_expense_schema = {
    "type": "object",
    "properties": {
        "amount": {
            "type": "number"
        },
        "name": {
            "type": "string"
        },
        "category": {
            "type": "string"
        },
        "date": {
            "type": "number"
        }
    },
    "additionalProperties": False
}

def validate_add_expense(data):
    try:
        validate(data, add_expense_schema)
    except (ValidationError, SchemaError) as e:
        return {'ok': False, 'message': e}

    return {'ok': True, 'data': data}
