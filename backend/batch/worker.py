''' periodic worker updates db transactions with mongo. '''
import os
import sys
import time
from pymongo import MongoClient
import bulk_update
from apscheduler.schedulers.blocking import BlockingScheduler
import logging


def log_to_stdout():
    logging.basicConfig()
    logging.getLogger('apscheduler').setLevel(logging.DEBUG)

def init_db():
    return MongoClient(os.environ.get('DB'))


def main():
    print("init db")
    mongo = init_db()
    db = mongo.get_database('todoDev')
    print("finished init db")

    print('initialize scheduler')
    scheduler = BlockingScheduler()
    log_to_stdout()


    #schedule the different workers
    scheduler.add_job(bulk_update.run_periodic, 'cron', hour=0, minute=30, args=[db])
    scheduler.add_job(bulk_update.run_incomes, 'cron', day=1, hour=0, minute=30, args=[db])
    scheduler.add_job(bulk_update.run_expenses, 'cron', day=1, hour=0, minute=30, args=[db])

    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        print('exit scheduler.')

if __name__ == '__main__':
    main()
