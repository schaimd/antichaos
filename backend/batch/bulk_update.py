from pymongo import UpdateOne, InsertOne
import time


def get_periodic_transactions(db):
    return db.periodic.aggregate([
       { "$match": { "left": { "$gt": 0 } } },
       { "$set": { "dayOfWeek": { "$dayOfWeek": { "$toDate": '$start_time' } } } } ,
       { "$set": { "dayOfWeekNow": { "$dayOfWeek": { "$toDate": '$$NOW' } } } },
       { "$set": { "dayOfMonth": { "$dayOfMonth": { "$toDate": '$start_time' } } } },
       { "$set": { "dayOfMonthNow": { "$dayOfMonth": { "$toDate": '$$NOW' } } } },
       { "$match": { "$or": [{ "period": 'daily'},
                         { "period": 'Weekly',
                           "$expr": { "$eq": ['$dayOfWeek', '$dayOfWeekNow'] } },
                         { "period": 'Monthly',
                           "$expr": { "$eq": ['$dayOfMonth', '$dayOfMonthNow'] } }
                           ] } },
       { "$project": { 'dayOfWeek':0, 'dayOfMonth':0,
                    'dayOfWeekNow':0, 'dayOfMonthNow': 0 } }
    ])


def perform_transactions(db, transactions, now):
    operations = []
    decreases = []
    records = []
    for transaction in transactions:
        # query the asset
        actual = transaction['transaction']
        if actual['name'] in ('TRANSFER', 'WITHDRAW'):
            source_query = {'email': transaction['email'],
                            'asset': actual['source']}
            action = {'$inc': {'amount': - actual['amount'] }}
            operations.append(UpdateOne(source_query, action))
        if actual['name'] in ('TRANSFER', 'DEPOSIT'):
            target_query = {'email': transaction['user'],
                            'asset': actual['target']}
            action = {'$inc': {'amount': actual['amount'] }}
        operations.append(UpdateOne(target_query, action))

        # decrease periods
        query = {'_id': transaction['_id']}
        action = {'$inc': { 'periods': -1 }}
        decreases.append(UpdateOne(query, action))

        # record it
        record = transaction.copy()
        record['email'] = transaction['email']
        record['timestamp'] = now
        record['tag'] = 'Periodic: ' + record['tag']
        records.append(InsertOne(record))

    if operations:
        db.assets.bulk_write(operations)
        db.periodic.bulk_write(decreases)
        db.history.bulk_write(records)
    else:
        print('no transactions to perform')


def run_periodic(db):
    now = time.time()
    transactions = get_periodic_transactions(db)
    perform_transactions(db, transactions, now)


def get_income_updates(db):
    return db.settings.aggregate([
       { '$match': { 'budget_configuration.income': { '$exists': 'true' },
                   'checking_account.asset': { '$exists': 'true' } } },
       { '$project': { 'asset': '$checking_account.asset',
                       'email':1,
                       'income':'$budget_configuration.income',
                       '_id': 0} }])


def deposit_salaries(db, salaries, now):
    operations = []
    records = []
    for salary in salaries:
        query = {
            'email': salary['email'],
            'asset': salary['asset']
        }
        action = {
            '$inc': {'amount': salary['income']}
        }
        operations.append(UpdateOne(query, action))
        records.append(InsertOne({
            'name' : 'DEPOSIT',
            'tag': 'Automatic: Salary',
            'amount': salary['income'],
            'to': salary['asset'],
            'email': salary['email'],
            'timestamp': now,
            }))
    if operations:
        db.assets.bulk_write(operations)
        db.history.bulk_write(records)
    else:
        print('no salaries to deposit')


def run_incomes(db):
    now = time.time()
    salaries = get_income_updates(db)
    deposit_salaries(db, salaries, now)


def get_last_month_expenses(db):
    return db.expenses.aggregate([
        { '$set': { 'min_date': {
                        '$divide': [
                            { '$toLong':
                                { '$add': ["$$NOW",
                                        { '$multiply': [-1000, 60, 60, 24, 30] }]
                                }
                            }, 1000 ] },
                  'this_month': { '$month': {'$toDate': {'$multiply': [1000, "$date"] } } },
                  'now_month': { '$month': "$$NOW" } } },
        { '$match': { '$and': [
                        { '$expr': {
                            '$eq': ['$this_month',
                                    { '$add': [1,
                                            {'$mod':
                                                 [{ '$add': ['$now_month', -2]},
                                                  12]
                                             }]
                                    }]
                             }
                        },
                        { '$expr': {
                            '$gt': [
                                '$min_date',
                                '$date']
                             }
                        }]}
         },
         {
            '$project': {
                'email': 1,
                'amount': 1
            }
         },
         {
            '$group': {
                  '_id' : "$email",
                  'expense': { '$sum':  "$amount" }
                  }
         },
         {
            '$lookup' : {
               'from': 'settings',
               'localField': '_id',
               'foreignField': 'email',
               'as': 'settings'
             }
         },
         { '$unwind': '$settings' },
         {
            '$project': {
                '_id': 0,
                'expense': 1,
                'email': '$_id',
                'asset': '$settings.checking_account.asset',
            }
         }
        ])


def withdraw_expenses(db, expenses, now):
    operations = []
    records = []
    for expense in expenses:
        query = {
            'email': expense['email'],
            'asset': expense['asset']
        }
        action = {
            '$inc': {'amount': -expense['expense']}
        }
        operations.append(UpdateOne(query, action))
        records.append(InsertOne({
            'name' : 'WITHDRAW',
            'tag': 'Automatic: monthly expenses',
            'amount': expense['expense'],
            'from': expense['asset'],
            'email': expense['email'],
            'timestamp': now,
        }))
    if operations:
        db.assets.bulk_write(operations)
        db.history.bulk_write(records)
    else:
        print('no expenses to withdraw')


def run_expenses(db):
    now = time.time()
    expenses = get_last_month_expenses(db)
    withdraw_expenses(db, expenses, now)
