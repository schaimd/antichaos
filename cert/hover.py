#!/usr/bin/env python

"""hover.py: Provides dynamic DNS functionality for Hover.com using their unofficial API.
   This script is based off one by Dan Krause: https://gist.github.com/dankrause/5585907"""

__author__      = "Andrew Barilla"
__credits__ = ["Andrew Barilla", "Dan Krause"]
__license__ = "GPL"
__version__ = "1.0"
__maintainer__ = "Andrew Barilla"
__email__ = "andrew@exit66.com"
__status__ = "Production"

import requests
import json
import os
import pickle

# Your hover.com username and password
TOKEN = os.environ["CERTBOT_VALIDATION"]

class HoverException(Exception):
    pass


class HoverAPI(object):
    def __init__(self, username, password):
        params = {"username": username, "password": password}
        r = requests.post("https://www.hover.com/api/login", json=params)
        if not r.ok or "hoverauth" not in r.cookies:
            raise HoverException(r)
        self.cookies = {"hoverauth": r.cookies["hoverauth"]}
    def call(self, method, resource, data=None):
        url = "https://www.hover.com/api/{0}".format(resource)
        r = requests.request(method, url, data=data, cookies=self.cookies)
        if not r.ok:
            raise HoverException(r)
        if r.content:
            body = r.json()
            if "succeeded" not in body or body["succeeded"] is not True:
                raise HoverException(body)
            return body
if __name__ == '__main__':
    # connect to the API using your account
    account = None
    with open("/app/account.txt", "rb") as f:
        account = pickle.load(f)
    client = HoverAPI(account["user"], account["password"])
    client.call("put", "dns/" + account["dns_id"], {"content": TOKEN})
