import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import { store } from './store/configureStore';
import { App } from './app';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import green from '@material-ui/core/colors/green';
import CssBaseline from '@material-ui/core/CssBaseline';

render(
    <Provider store={store}>
        <MuiThemeProvider>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
            <CssBaseline />
            <App />
        </MuiThemeProvider>
    </Provider>,
    document.getElementById('app')
);
