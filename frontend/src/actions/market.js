import { marketConstants } from '../constants/market';
import { marketServices } from '../services/market';
import { alertActions } from './alerts';

function getMarketPrice(symbols) {
    return dispatch => {
        dispatch(request(symbols));

        marketServices.get_market_price(symbols)
            .then(
                market_prices => dispatch(success(market_prices)),
                error => dispatch(failure(error.toString()))
            );
    }
    function request(symbols) { return { type: marketConstants.GET_REQUEST, symbols } }
    function success(market_prices) { return { type: marketConstants.GET_SUCCESS, market_prices} }
    function failure(error) { return { type: marketConstants.GET_FAILURE, error } }
}

export const marketActions = {
    getMarketPrice,
};
