import { assetConstants } from '../constants/asset';
import { assetServices } from '../services/asset';
import { alertActions } from './alerts';

function addAsset(name, amount) {
    return dispatch => {
        dispatch(request(name, amount));

        assetServices.add_asset(name, amount)
            .then(
                _ => {
                    dispatch(success());
                    dispatch(alertActions.success('Asset added successfuly'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request(name, amount) { return { type: assetConstants.ADD_REQUEST, name, amount } }
    function success(users) { return { type: assetConstants.ADD_SUCCESS} }
    function failure(error) { return { type: assetConstants.ADD_FAILURE, error } }
}


function RemoveAsset(name) {
    return dispatch => {
        dispatch(request(name));

        assetServices.remove_asset(name)
            .then(
                _ => {
                    dispatch(success());
                    dispatch(alertActions.success('Asset removed successfuly'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request(name) { return { type: assetConstants.REMOVE_REQUEST } }
    function success(users) { return { type: assetConstants.REMOVE_SUCCESS} }
    function failure(error) { return { type: assetConstants.REMOVE_FAILURE, error } }
}


function getAssets() {
    return dispatch => {
        dispatch(request());

        assetServices.get_assets()
            .then(
                assets => dispatch(success(assets)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: assetConstants.GETALL_REQUEST } }
    function success(assets) { return { type: assetConstants.GETALL_SUCCESS, assets } }
    function failure(error) { return { type: assetConstants.GETALL_FAILURE, error } }
}

function getAssetHistory(asset) {
    return dispatch => {
        dispatch(request(asset));

        assetServices.get_asset_history(asset)
            .then(
                history => dispatch(success(history)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: assetConstants.GET_HISTORY_REQUEST } }
    function success(history) { return { type: assetConstants.GET_HISTORY_SUCCESS, history } }
    function failure(error) { return { type: assetConstants.GET_HISTORY_FAILURE, error } }
}

export const assetActions = {
    addAsset,
    RemoveAsset,
    getAssets,
    getAssetHistory,
};
