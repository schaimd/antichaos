import { contractConstants } from '../constants/contract';
import { contractServices } from '../services/contract';
import { alertActions } from './alerts';

function addContract(contract) {
    return dispatch => {
        dispatch(request(contract));

        contractServices.add_contract(contract)
            .then(
                _ => {
                    dispatch(success());
                    dispatch(alertActions.success('Contract added successfuly'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request(contract) { return { type: contractConstants.ADD_REQUEST, contract } }
    function success(users) { return { type: contractConstants.ADD_SUCCESS} }
    function failure(error) { return { type: contractConstants.ADD_FAILURE, error } }
}

function getContract() {
    return dispatch => {
        dispatch(request());

        contractServices.get_contract()
            .then(
                assets => dispatch(success(assets)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: contractConstants.GET_REQUEST } }
    function success(contract) { return { type: contractConstants.GET_SUCCESS, contract } }
    function failure(error) { return { type: contractConstants.GET_FAILURE, error } }
}

export const contractActions = {
    addContract,
    getContract,
};
