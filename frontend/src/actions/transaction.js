import { transactionConstants } from '../constants/transaction';
import { transactionServices } from '../services/transaction';
import { alertActions } from './alerts';


function withdraw(name, amount, tag) {
    return dispatch => {
        dispatch(request());

        transactionServices.withdraw(name, amount, tag)
            .then(
                _ => {
                    dispatch(success());
                    dispatch(alertActions.success('withdrew successfuly'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request() { return { type: transactionConstants.TRANSACTION_REQUEST} }
    function success() { return { type: transactionConstants.TRANSACTION_SUCCESS} }
    function failure(error) { return { type: transactionConstants.TRANSACTION_FAILURE, error } }
}


function deposit(name, amount, tag) {
    return dispatch => {
        dispatch(request());

        transactionServices.deposit(name, amount, tag)
            .then(
                _ => {
                    dispatch(success());
                    dispatch(alertActions.success('deposited successfuly'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request() { return { type: transactionConstants.TRANSACTION_REQUEST} }
    function success() { return { type: transactionConstants.TRANSACTION_SUCCESS} }
    function failure(error) { return { type: transactionConstants.TRANSACTION_FAILURE, error } }
}


function transfer(source, target, amount, tag) {
    return dispatch => {
        dispatch(request());

        transactionServices.transfer(source, target, amount, tag)
            .then(
                _ => {
                    dispatch(success());
                    dispatch(alertActions.success('transfer successfuly'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    }
    function request() { return { type: transactionConstants.TRANSACTION_REQUEST} }
    function success() { return { type: transactionConstants.TRANSACTION_SUCCESS} }
    function failure(error) { return { type: transactionConstants.TRANSACTION_FAILURE, error } }
}


function sendPeriodTransaction(transaction) {
    return dispatch => {
        dispatch(request());

        transactionServices.send_periodic_transaction(transaction)
            .then(
                _ => dispatch(success()),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: transactionConstants.TRANSACTION_REQUEST } }
    function success() { return { type: transactionConstants.TRANSACTION_SUCCESS } }
    function failure(error) { return { type: transactionConstants.TRANSACTION_FAILURE, error } }
}


function getPeriodicTransactions() {
    return dispatch => {
        dispatch(request());

        transactionServices.get_periodic_transactions()
            .then(
                transactions => dispatch(success(transactions)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: transactionConstants.GETALL_REQUEST } }
    function success(transactions) { return { type: transactionConstants.GETALL_SUCCESS, transactions } }
    function failure(error) { return { type: transactionConstants.GETALL_FAILURE, error } }
}

export const transactionActions = {
    withdraw,
    deposit,
    transfer,
    getPeriodicTransactions,
    sendPeriodTransaction
};
