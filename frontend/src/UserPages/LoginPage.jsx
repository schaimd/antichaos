import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../actions/user';

import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import GoogleLogin from 'react-google-login';

import login_style from './common_paper_style'


class Login extends React.Component {
    constructor(props){
        super(props);
        this.props.dispatch(userActions.logout());
        this.state = {
                username:'',
                password:''
            }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.verify_login = this.verify_login.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();

        const { username, password } = this.state;
        const { dispatch } = this.props;
        dispatch(userActions.login(username, password));
    }

    verify_login(response) {
        this.props.dispatch(
            userActions.googleLogin(response.tokenId,
                                    response.profileObj.email,
                                    response.profileObj.givenName));
    }

    render() {
        const { classes, theme } = this.props;

        return (
            <main className={classes.main}>
              <CssBaseline />
              <Paper className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <Icon> locked_icon </Icon>
                </Avatar>
                <Typography component="h1" variant="h5">
                  Sign in
                </Typography>
                <Grid container direction='column' spacing={3}>
                <Grid item>
                    <form className={classes.form} onSubmit={this.handleSubmit} >
                      <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="email">Email Address</InputLabel>
                        <Input
                            id="email"
                            name="email"
                            autoComplete="email"
                            onChange={e => this.setState({username:e.target.value})}
                            autoFocus  />
                      </FormControl>
                      <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="password">Password</InputLabel>
                        <Input
                            name="password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={e => this.setState({password:e.target.value})}/>
                      </FormControl>
                      <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.button}>
                        Sign in
                      </Button>
                      <div style={{paddingTop: 15}}>
                      <Link to="/register">I'm not in the matrix, Register me.</Link>
                      </div>
                      </form>
                  </Grid>
                  <Grid item>
                      <GoogleLogin
                        clientId="464194612753-0dgbfa559vhhapv9r4t705irceva72tv.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={this.verify_login}
                        cookiePolicy={'single_host_origin'}
                      />
                  </Grid>
                  </Grid>
              </Paper>
            </main>
    );
  }
}


function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

Login.propTypes = {
  classes: PropTypes.object.isRequired,
};

const connectedLoginPage = connect(mapStateToProps)(Login);
export default withStyles(login_style)(connectedLoginPage);
