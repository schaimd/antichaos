import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../actions/user';

import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import login_style from './common_paper_style'

class Register extends React.Component {
    constructor(props){
        super(props);
        this.props.dispatch(userActions.logout());
        this.state = {
          name:'',
          email:'',
          password:''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const { dispatch } = this.props;
        dispatch(userActions.register(this.state));
    }
    render() {
        const { classes, theme } = this.props;

        return (
            <main className={classes.main}>
              <CssBaseline />
              <Paper className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <Icon> account_circle </Icon>
                </Avatar>
                <Typography component="h1" variant="h5">
                  Registration Form
                </Typography>
                <form className={classes.form} onSubmit={this.handleSubmit} >
                  <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="name">Name</InputLabel>
                    <Input
                        id="name"
                        name="name"
                        autoComplete="name"
                        onChange={e => this.setState({name:e.target.value})}
                        autoFocus  />
                  </FormControl>
                  <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="email">Email Address</InputLabel>
                    <Input
                        id="email"
                        name="email"
                        autoComplete="email"
                        onChange={e => this.setState({email:e.target.value})}
                        autoFocus  />
                  </FormControl>
                  <FormControl margin="normal" required fullWidth>
                    <InputLabel htmlFor="password">Password</InputLabel>
                    <Input
                        name="password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onChange={e => this.setState({password:e.target.value})}/>
                  </FormControl>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.button}>
                    Register
                  </Button>
                  <Button
                  component={Link}
                  to="/login"
                  fullWidth
                  variant="contained"
                  color="secondary"
                  className={classes.button}>
                      Cancel
                  </Button>
                </form>
              </Paper>
            </main>
    );
  }
}


function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedRegisterPage = connect(mapStateToProps)(Register);
export default withStyles(login_style)(connectedRegisterPage);
