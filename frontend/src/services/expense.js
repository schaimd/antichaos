import { authHeader } from '../helpers/auth-header';
import { userService } from './user';

export const expenseServices = {
    add_expense,
    remove_expense,
    get_expenses,
    get_monthly_expenses,
};


function add_expense(name, amount, category, date) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ name, amount, category, date })
    };

    return fetch('/data/expense', requestOptions).then(handleResponse)
}


function remove_expense(name, amount, category, date) {
    const requestOptions = {
        method: 'DELETE',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ name, amount, category, date })
    };

    return fetch('/data/expense', requestOptions).then(handleResponse)
}


function get_expenses(date) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch('/data/expenses?date=' + date, requestOptions)
        .then(handleResponse)
        .then(response => {
            return response;
        });
}

function get_monthly_expenses(year) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch('/data/monthly_expenses?year=' + year, requestOptions)
        .then(handleResponse)
        .then(response => {
            return response;
        });
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                userService.logout();
                window.location.reload(true);
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data.data;
    });
}
