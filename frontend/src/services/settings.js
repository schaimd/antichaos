import { authHeader } from '../helpers/auth-header';
import { userService } from './user';

export const settingsServices = {
    get_budget,
    set_budget,
    get_checking_account,
    set_checking_account,
};


function set_budget(income, budget) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ income, budget })
    };

    return fetch('/data/set_budget', requestOptions).then(handleResponse)
}

function get_budget() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch('/data/get_budget', requestOptions)
        .then(handleResponse)
        .then(response => {
            return response;
        });
}

function set_checking_account(asset, date) {
    const requestOptions = {
        method: 'POST',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify({ asset, date })
    };

    return fetch('/data/set_checking_account', requestOptions).then(handleResponse)
}

function get_checking_account() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch('/data/get_checking_account', requestOptions)
        .then(handleResponse)
        .then(response => {
            return response;
        });
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                userService.logout();
                window.location.reload(true);
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data.data;
    });
}
