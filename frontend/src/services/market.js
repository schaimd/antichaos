import { authHeader } from '../helpers/auth-header';
import { userService } from './user';

export const marketServices = {
    get_market_price,
};


function get_market_price(symbols) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ ...symbols })
    };

    return fetch('/data/market', requestOptions).then(handleResponse)
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        console.log("got response");
        console.log(response);
        if (!response.ok) {
            console.log("not ok response");
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        console.log("finished");

        return data.data;
    });
}
