import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { transactionActions } from '../actions/transaction';
import Hidden from '@material-ui/core/Hidden';
import Grid from "@material-ui/core/Grid";


const styles = theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
    marginTop: theme.spacing.unit * 2,
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  },
  table: {
    minWidth: 700,
  },
});


class SimpleTable extends React.Component {
    componentDidMount() {
        this.props.dispatch(transactionActions.getPeriodicTransactions());
    }

    componentDidUpdate(PrevProps, PrevStates) {
        const { transaction: prev_transaction } = PrevProps;
        const { transaction: next_transaction } =  this.props;
        if (prev_transaction.sending_action &&
                next_transaction.action_performed) {
            this.props.dispatch(transactionActions.getPeriodicTransactions());
        }
    }

    render_mobile(classes, transactions) {
        return (
            <div>
            {transactions.items && transactions.items.map(n => {
             var t = new Date(n.start_time);
             var f = t.toLocaleDateString();
             return (
                 <ExpansionPanel>
                   <ExpansionPanelSummary
                     expandIcon={<ExpandMoreIcon />}
                     aria-controls={"panel" + n.id + "a-content"}
                     id={"panel" + n.id + "a-header"}
                   >
                     <Typography className={classes.heading}>{n.transaction.tag}</Typography>
                   </ExpansionPanelSummary>
                   <ExpansionPanelDetails>
                     <Grid container spacing={2}>
                        <Grid item>
                        { "Type: " + n.transaction.name }
                        </Grid>
                        <Grid item>
                        { "Source: " + n.transaction.source }
                        </Grid>
                        <Grid item>
                        { "Target: " + n.transaction.target }
                        </Grid>
                        <Grid item>
                        { "Amount: " + n.transaction.amount.toLocaleString() }
                        </Grid>
                        <Grid item>
                        { "Date: " + f }
                        </Grid>
                        <Grid item>
                        { "Total Period: " + n.period }
                        </Grid>
                        <Grid item>
                        { "Periods Left: " + n.left }
                        </Grid>
                     </Grid>
                   </ExpansionPanelDetails>
                 </ExpansionPanel>
              );
            })}
            </div>
        );
    }

    render_desktop(classes, transactions) {
        return (
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="right">Action</TableCell>
              <TableCell align="right">From</TableCell>
              <TableCell align="right">To</TableCell>
              <TableCell align="right">Amount</TableCell>
              <TableCell align="right">Start Time</TableCell>
              <TableCell align="right">Period</TableCell>
              <TableCell align="right">Left</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {transactions.items && transactions.items.map(n => {
             var t = new Date(n.start_time);
             var f = t.toLocaleDateString();
             return (
                <TableRow key={n.id}>
                  <TableCell component="th" scope="row">
                    {n.transaction.tag}
                  </TableCell>
                  <TableCell align="right">{n.transaction.name}</TableCell>
                  <TableCell align="right">{n.transaction.source}</TableCell>
                  <TableCell align="right">{n.transaction.target}</TableCell>
                  <TableCell align="right">{n.transaction.amount.toLocaleString()}</TableCell>
                  <TableCell align="right">{f}</TableCell>
                  <TableCell align="right">{n.period}</TableCell>
                  <TableCell align="right">{n.left}</TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
        );
    }
    render() {
        const { classes, transactions } = this.props;

        return (
          <Paper className={classes.root}>
          <Hidden xsDown implementation="css">
            { this.render_desktop(classes, transactions) }
          </Hidden>
          <Hidden smUp implementation="css">
            { this.render_mobile(classes, transactions) }
          </Hidden>
          </Paper>
        );
    }
}


SimpleTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const { transactions, transaction } = state;
    return {
        transaction,
        transactions,
    };
}

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps)(SimpleTable));
