import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import PeriodicDialog from './PeriodicStepper';
import PeriodicTable from './PeriodicTable';
import Layout from '../Layout/Layout';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';


export class PeriodPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };

        this.renderAddButton = this.renderAddButton.bind(this);
    }

    renderAddButton() {
        return (
            <div>
                <Fab
                    color="secondary"
                    size="small"
                    aria-label="Add Asset"
                    onClick={e => this.setState({ open: true })}>
                  <Icon> add_icon </Icon>
                </Fab>
            </div>
        )
    }

    render() {
        return (
            <Layout PageName="Repeated Transactions" AppBarIcons={this.renderAddButton}>
                {this.state.open &&
                        <PeriodicDialog handleClose={() => this.setState({ open: false})} />
                }
                
                <PeriodicTable />
            </Layout>
        )
    }
}
