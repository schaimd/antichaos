import { transactionConstants } from '../constants/transaction';

export function transaction(state = {}, action) {
  switch (action.type) {
    case transactionConstants.TRANSACTION_REQUEST:
      return {
        sending_action: true
      };
    case transactionConstants.TRANSACTION_SUCCESS:
      return {
        action_performed: true
      };
    case transactionConstants.TRANSACTION_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}

export function transactions(state = {}, action) {
  switch (action.type) {
    case transactionConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case transactionConstants.GETALL_SUCCESS:
      return {
        items: action.transactions
      };
    case transactionConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}
