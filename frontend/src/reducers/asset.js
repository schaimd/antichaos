import { assetConstants } from '../constants/asset';

export function assets(state = {}, action) {
  switch (action.type) {
    case assetConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case assetConstants.GETALL_SUCCESS:
      return {
        items: action.assets
      };
    case assetConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}

export function asset_history(state = {}, action) {
  switch (action.type) {
    case assetConstants.GET_HISTORY_REQUEST:
      return {
        loading: true
      };
    case assetConstants.GET_HISTORY_SUCCESS:
      return {
        items: action.history
      };
    case assetConstants.GET_HISTORY_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}

export function addasset(state = {}, action) {
  switch (action.type) {
    case assetConstants.ADD_REQUEST:
      return {
        adding: true
      };
    case assetConstants.ADD_SUCCESS:
      return {
        added: true
      };
    case assetConstants.ADD_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}


export function removeasset(state = {}, action) {
  switch (action.type) {
    case assetConstants.REMOVE_REQUEST:
      return {
        removing: true
      };
    case assetConstants.REMOVE_SUCCESS:
      return {
        removed: true
      };
    case assetConstants.REMOVE_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}
