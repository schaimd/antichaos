import { contractConstants } from '../constants/contract';

export function contract(state = {}, action) {
  switch (action.type) {
    case contractConstants.GET_REQUEST:
      return {
        loading: true
      };
    case contractConstants.GET_SUCCESS:
      return {
        contract: action.contract
      };
    case contractConstants.GET_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}

export function add_contract(state = {}, action) {
  switch (action.type) {
    case contractConstants.ADD_REQUEST:
      return {
        adding: true
      };
    case contractConstants.ADD_SUCCESS:
      return {
        added: true
      };
    case contractConstants.ADD_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}
