import { marketConstants } from '../constants/market';

export function market_prices(state = {}, action) {
  switch (action.type) {
    case marketConstants.GET_REQUEST:
      return {
        symbols: action.symbols,
        loading: true
      };
    case marketConstants.GET_SUCCESS:
      return {
        prices: action.market_prices
      };
    case marketConstants.GET_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}
