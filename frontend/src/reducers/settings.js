import { settingsConstants } from '../constants/settings';

export function get_budget(state = {}, action) {
  switch (action.type) {
    case settingsConstants.GET_BUDGET_REQUEST:
      return {
        loading: true
      };
    case settingsConstants.GET_BUDGET_SUCCESS:
      return {
        budget: action.budget,
      };
    case settingsConstants.GET_BUDGET_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}

export function set_budget(state = {}, action) {
  switch (action.type) {
    case settingsConstants.SET_BUDGET_REQUEST:
      return {
        setting: true
      };
    case settingsConstants.SET_BUDGET_SUCCESS:
      return {
        set: true
      };
    case settingsConstants.SET_BUDGET_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}

export function get_checking_account(state = {}, action) {
  switch (action.type) {
    case settingsConstants.GET_CHECKING_ACCOUNT_REQUEST:
      return {
        loading: true
      };
    case settingsConstants.GET_CHECKING_ACCOUNT_SUCCESS:
      return {
        checking_account: action.checking_account,
      };
    case settingsConstants.GET_CHECKING_ACCOUNT_FAILURE:
      return {
        error: action.error
      };
    default:
        return state;
    }
}


export function set_checking_account(state = {}, action) {
    switch (action.type) {
      case settingsConstants.SET_CHECKING_ACCOUNT_REQUEST:
        return {
          setting: true
        };
      case settingsConstants.SET_CHECKING_ACCOUNT_SUCCESS:
        return {
          set: true
        };
      case settingsConstants.SET_CHECKING_ACCOUNT_FAILURE:
        return {
          error: action.error
        };
      default:
          return state;
      }
}
