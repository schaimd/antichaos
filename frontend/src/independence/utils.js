function calculate_target_sum(monthly_expense) {
    return monthly_expense * 400;
}

function get_monthly_interest(r_year) {
    return  ((1 + r_year) ** (1/12)) - 1;
}

function get_months_to_freedom(PV, pmt, r_year, FV) {
    const r_month = get_monthly_interest(r_year);
    // FV = PV * (1 + r) ^ n + (p / r) * ((1 + r) ^ n - 1)
    // => (FV * r + p)/ (PV * r + p) = (1 + r) ^ n
    // => n = log([(FV * r + p) / (PV * r + p)]) / log(1 + r)
    const to_logarithm = (FV * r_month  + pmt) /  (PV * r_month + pmt);
    return Math.log(to_logarithm) / Math.log(1 + r_month);
}


export const FinanceUtils = {
    calculate_target_sum,
    get_monthly_interest,
    get_months_to_freedom,
};
