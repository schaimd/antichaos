import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';

import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import ArrowIcon from '@material-ui/icons/ArrowRightAlt';
import DeleteIcon from '@material-ui/icons/Delete';
import HistoryIcon from '@material-ui/icons/History';

import { assetActions } from '../actions/asset';


const styles = theme => ({
  card: {
      padding: theme.spacing.unit,
      margin: theme.spacing.unit * 2,
  },
  actions: {
      display: 'flex',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  positive: {
      color: green[300],
      fontSize: '3rem',
  },
  negative: {
      color: red[700],
      fontSize: '3rem',
  },
});


class AssetCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: this.props.amount
        }
        this.name = this.props.name;
        this.deleteAsset = this.deleteAsset.bind(this);
    }

    display_pretty_amount(positive, negative) {
        if (this.state.amount > 0) {
            return (<div className={positive}> {this.state.amount.toLocaleString()} </div>);
        }

        return (<div className={negative}> {this.state.amount.toLocaleString()} </div>);
    }

    deleteAsset() {
        const { dispatch } = this.props;
        dispatch(assetActions.RemoveAsset(this.name));
    }

    render() {
        const { classes } = this.props;
        return (<Card className={classes.card}>
            <CardHeader
              avatar={
                <Avatar aria-label="Asset" className={classes.avatar}>
                  A
                </Avatar>
              }
              title={this.name}

              action={
                  <IconButton
                      aria-label="Delete Icon"
                      onClick={this.deleteAsset}>
                    <DeleteIcon />
                  </IconButton>
              }
            />
          <CardContent>
            {this.display_pretty_amount(classes.positive, classes.negative)}
          </CardContent>
          <CardActions className={classes.actions} disableActionSpacing>
              <IconButton
                  aria-label="Deposit Cash"
                  onClick={this.props.Deposit(this.name)}>
                <AddIcon />
              </IconButton>
              <IconButton
                  aria-label="Withdraw Cash"
                  onClick={this.props.Withdraw(this.name)}>
                <RemoveIcon />
              </IconButton>
              <IconButton
                  aria-label="Transfer Cash"
                  onClick={this.props.Transfer(this.name)}>
                <ArrowIcon />
              </IconButton>
              <IconButton
                aria-label="history"
                onClick={this.props.ExpandHistory(this.name)}>
                <HistoryIcon />
              </IconButton>
          </CardActions>
        </Card>
        );
    }
}


AssetCard.propTypes = {
  name: PropTypes.element.isRequired,
  amount: PropTypes.element.isRequired,
  Withdraw: PropTypes.element.isRequired,
  Deposit: PropTypes.element.isRequired,
  Transfer: PropTypes.element.isRequired,
  ExpandHistory: PropTypes.element.isRequired,
};

export default withStyles(styles)(connect()(AssetCard));
