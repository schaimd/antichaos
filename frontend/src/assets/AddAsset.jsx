import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import Grid from '@material-ui/core/Grid';

import { assetActions } from '../actions/asset';


class AddAssetDialog extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          name: '',
          amount: 0,
      }

      this.handleAdd = this.handleAdd.bind(this);
  }

  handleAdd() {
    this.props.handleClose();
    const { dispatch } = this.props;
    const { name, amount } = this.state;
    // TODO: verify name isn't empty. else send alert
    dispatch(assetActions.addAsset(name, amount));
  }

  render() {
      const { fullScreen } = this.props;
      return (
        <div>
          <Dialog
            fullScreen={fullScreen}
            open={this.props.open}
            onClose={this.props.handleClose}
            aria-labelledby="add-asset-dialog"
          >
            <DialogTitle id="add-asset">Add Asset</DialogTitle>
            <DialogContent style={{display: 'flex', flexDirection: 'column'}}>
              <DialogContentText>
                Please select the Name and Amount of the new asset.
              </DialogContentText>
              <Grid
                container
                direction="column"
                justify="center"
                alignItems="flex-start"
                spacing={3}>
                <Grid item>
              <TextField
                autoFocus
                id="name"
                label="asset name"
                onChange={e => this.setState({ name : e.target.value})}
                type="text"
              />
              </Grid>
              <Grid item>
              <TextField
                autoFocus
                id="amount"
                label="new asset current amount"
                onChange={e => this.setState({ amount : Number(e.target.value)})}
                type="number"
              />
              </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.props.handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={this.handleAdd} color="primary">
                Add
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      );
    }
}

AddAssetDialog.propTypes = {
  open: PropTypes.element.isRequired,
  handleClose: PropTypes.element.isRequired
};

const connectedDialog = connect()(withMobileDialog()(AddAssetDialog));
export default connectedDialog;
