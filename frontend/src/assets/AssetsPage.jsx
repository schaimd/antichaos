import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';

import Assets from './Assets';
import AssetHistory from './AssetHistory';
import AddAssetDialog from './AddAsset';
import Layout from '../Layout/Layout';
import { SimpleTransactionDialog, TransferDialog } from './Transactions';

import { transactionActions } from '../actions/transaction'


export class AssetsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            add: {
                open: false
            },
            deposit: {
                open: false,
                name: ''
            },
            withdraw: {
                open: false,
                name: ''
            },
            transfer: {
                open: false,
                name: ''
            },
            expand_history: {
                open: false,
                name: ''
            }
        }

        this.handleOpenAdd = this.handleOpenAdd.bind(this);
        this.handleCloseAdd = this.handleCloseAdd.bind(this);

        this.handleOpenWithdraw = this.handleOpenWithdraw.bind(this);
        this.handleOpenDeposit = this.handleOpenDeposit.bind(this);
        this.handleOpenTransfer = this.handleOpenTransfer.bind(this);
        this.handleCloseWithdraw = this.handleCloseWithdraw.bind(this);
        this.handleCloseDeposit = this.handleCloseDeposit.bind(this);
        this.handleCloseTransfer = this.handleCloseTransfer.bind(this);
        this.renderAddButton = this.renderAddButton.bind(this);
        this.ExpandHistory = this.ExpandHistory.bind(this);
        this.closeHistory = this.closeHistory.bind(this);
    }

    handleOpenAdd() {
      var add = { open: true }
      this.setState({ add });
    }

    handleCloseAdd() {
      var add = { open: false }
      this.setState({ add });
    }

    handleOpenWithdraw (name) {
        return () => {
            var withdraw = {
                    open: true,
                    name: name
            };
            console.log("logging withdraw");
            this.setState({ withdraw });
        }
    }

    handleOpenDeposit(name) {
        return () => {
            var deposit = {
                    open: true,
                    name: name
            };
            console.log("logging deposit");
            this.setState({ deposit });
        }
    }

    handleOpenTransfer(name) {
        return () => {
            var transfer = {
                    open: true,
                    name: name
            };
            console.log("logging transfer");
            this.setState({ transfer });
        }
    }

    handleCloseWithdraw() {
        console.log("logging withdraw close");
        var withdraw = { ...this.state.withdraw };
        withdraw.open = false;
        this.setState({ withdraw });
    }

    handleCloseDeposit() {
        console.log("logging deposit close");
        var deposit = { ...this.state.deposit };
        deposit.open = false;
        this.setState({ deposit });
    }

    handleCloseTransfer() {
        console.log("logging transfer close");
        var transfer = { ...this.state.transfer };
        transfer.open = false;
        this.setState({ transfer });
    }

    renderAddButton() {
        return (
            <div>
                <AddAssetDialog
                    open={this.state.add.open}
                    handleClose={this.handleCloseAdd} />
                <Fab
                    color="secondary"
                    size="small"
                    aria-label="Add Asset"
                    onClick={e => this.handleOpenAdd()}>
                  <Icon> add_icon </Icon>
                </Fab>
            </div>
        )
    }

    ExpandHistory(asset) {
        return () => {
            var expand_history = {
                open: true,
                name: asset
            }
            console.log('expand history with ' + asset)
            this.setState({ expand_history });
        }
    }

    closeHistory() {
        console.log('close history issued.')
        var expand_history = { open: false, name: '' }
        this.setState({expand_history})
    }

    render() {
        return (
            <Layout AppBarIcons={this.renderAddButton} PageName="Assets">
                <div>
                    <SimpleTransactionDialog
                        open={this.state.withdraw.open}
                        handleClose={this.handleCloseWithdraw}
                        AssetName={this.state.withdraw.name}
                        Transaction={transactionActions.withdraw}
                        TransactionName="Withdraw"
                    />
                    <SimpleTransactionDialog
                       open={this.state.deposit.open}
                       handleClose={this.handleCloseDeposit}
                       AssetName={this.state.deposit.name}
                       Transaction={transactionActions.deposit}
                       TransactionName="Deposit"
                    />
                    <TransferDialog
                       open={this.state.transfer.open}
                       handleClose={this.handleCloseTransfer}
                       AssetName={this.state.transfer.name}
                    />
                    <Assets
                        Withdraw={this.handleOpenWithdraw}
                        Deposit={this.handleOpenDeposit}
                        Transfer={this.handleOpenTransfer}
                        ExpandHistory={this.ExpandHistory}
                    />
                    {
                        this.state.expand_history.open && (
                            <AssetHistory
                                asset={this.state.expand_history.name}
                                closeHistory={this.closeHistory}
                            />
                        )
                    }
                </div>
            </Layout>
        )
    }
}
