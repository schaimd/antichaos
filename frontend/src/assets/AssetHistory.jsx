import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from "@material-ui/core/Grid";
import Hidden from '@material-ui/core/Hidden';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
import { assetActions } from '../actions/asset';
import fromUnixTime from 'date-fns/fromUnixTime';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Tooltip from '@material-ui/core/Tooltip';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';

const styles = theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
    marginTop: theme.spacing.unit * 2,
  },
  table: {
    minWidth: 700,
  },
  header: {
    width: "fit-content",
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.info.light,
    color: "white",
    "& svg": {
     margin: theme.spacing(1.5)
    },
    "& hr": {
     margin: theme.spacing(0, 0.5)
        }
    },
  positive: {
    color: green[300],
    },
  negative: {
    color: red[700],
    },
});

class SimpleTable extends React.Component {
    componentDidMount() {
        this.props.dispatch(assetActions.getAssetHistory(this.props.asset));
    }

    componentDidUpdate(PrevProps, PrevStates) {
        console.log('start update');
        const { asset: prev_asset } = PrevProps;
        const { asset: next_asset } =  this.props;
        if (prev_asset != next_asset) {
            console.log('asset changed, rereading history');
            this.props.dispatch(assetActions.getAssetHistory(this.props.asset));
        }
        const { transaction: prev_transaction } = PrevProps;
        const { transaction: next_transaction } = this.props;
        if ( prev_transaction.sending_action &&
             next_transaction.action_performed) {
            console.log('transaction added, rereading history');
            this.props.dispatch(assetActions.getAssetHistory(this.props.asset));
        }
    }

    colorize(transaction) {
        const {classes} = this.props;
        if (this.props.asset == transaction.target) {
            return (<div className={classes.positive}> {transaction.amount.toLocaleString()} </div>)
        }
        return (<div className={classes.negative}> {transaction.amount.toLocaleString()} </div>)
    }

    render_mobile(classes, history) {
        return (
            <div>
            {history.items && history.items.map(n => {
             var t = fromUnixTime(n.timestamp);
             var f = t.toLocaleDateString();
             return (
                 <ExpansionPanel>
                   <ExpansionPanelSummary
                     expandIcon={<ExpandMoreIcon />}
                     aria-controls={"panel" + n.id + "a-content"}
                     id={"panel" + n.id + "a-header"}
                   >
                     <Typography className={classes.heading}>
                        <div>
                        <div>
                            {n.tag + '  '}
                        </div>
                        {this.colorize(n)}
                        </div>
                     </Typography>
                   </ExpansionPanelSummary>
                   <ExpansionPanelDetails>
                     <Grid container spacing={2}>
                        <Grid item>
                        { "Type: " + n.name }
                        </Grid>
                        <Grid item>
                        { "Date: " + f}
                        </Grid>
                     </Grid>
                   </ExpansionPanelDetails>
                 </ExpansionPanel>
              );
            })}
            </div>
        );
    }

    render_desktop(classes, history) {
        return (
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="right">Amount</TableCell>
                  <TableCell align="right">Type</TableCell>
                  <TableCell align="right">Date</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {history.items && history.items.map(n => {
                 var t = fromUnixTime(n.timestamp);
                 var f = t.toLocaleDateString();
                 return (
                    <TableRow>
                      <TableCell>{n.tag}</TableCell>
                      <TableCell align="right">{this.colorize(n)}</TableCell>
                      <TableCell align="right">{n.name}</TableCell>
                      <TableCell align="right">{f}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
        );
    }

    render() {
        const { classes, asset_history, asset } = this.props;
        return (
        <div>
        <Toolbar>
              <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                {asset}
              </Typography>
              <Tooltip title="close">
                <IconButton aria-label="close" onClick={this.props.closeHistory}>
                  <CloseIcon />
                </IconButton>
              </Tooltip>
          </Toolbar>
          <Paper className={classes.root}>
              <Hidden xsDown implementation="css">
                { this.render_desktop(classes, asset_history) }
              </Hidden>
              <Hidden smUp implementation="css">
                { this.render_mobile(classes, asset_history) }
              </Hidden>
          </Paper>
          </div>
        );
    }
}


SimpleTable.propTypes = {
  classes: PropTypes.object.isRequired,
  asset: PropTypes.object.isRequired,
  closeHistory: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const {  authentication, asset_history, transaction } = state;
    return {
        asset_history,
        transaction,
    };
}

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps)(SimpleTable));
