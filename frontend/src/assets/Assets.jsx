import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';

import { assetActions } from '../actions/asset';
import AssetCard from './AssetCard';
import Card from '@material-ui/core/Card';



class Assets extends React.Component {
    componentDidMount() {
        this.props.dispatch(assetActions.getAssets());
    }

    componentDidUpdate(PrevProps, PrevStates) {
        const { addasset: prev_addasset } = PrevProps;
        const { addasset: next_addasset } =  this.props;
        if (prev_addasset.adding && next_addasset.added) {
            this.props.dispatch(assetActions.getAssets());
        }

        const { removeasset: prev_removeasset } = PrevProps;
        const { removeasset: next_removeasset } =  this.props;
        if (prev_removeasset.removing && next_removeasset.removed) {
            this.props.dispatch(assetActions.getAssets());
        }

        const { transaction: prev_transaction } = PrevProps;
        const { transaction: next_transaction } =  this.props;
        if (prev_transaction.sending_action &&
                next_transaction.action_performed) {
            this.props.dispatch(assetActions.getAssets());
        }
    }

    renderCards(assets) {
        return Object.keys(assets.items).map(
                    asset => (<Grid item xs={12} sm={4}>
                                <AssetCard
                                    name={asset}
                                    amount={assets.items[asset]}
                                    Deposit={this.props.Deposit}
                                    Withdraw={this.props.Withdraw}
                                    Transfer={this.props.Transfer}
                                    ExpandHistory={this.props.ExpandHistory} />
                              </Grid>))
    }

    renderEmpty() {
        return (
        <Grid item xs={12} sm={6}>
            <Card style={{margin: 15, padding: 10, fontSize:30}}>
              You don't have any assets.
            </Card>
        </Grid>)
    }

    render() {
        const { user, assets } = this.props;
        return (
            <div>
                {assets.loading ? (
                    <div>
                        <em>Loading assets...</em>
                        <CircularProgress style={{margin: 16}} />
                     </div>)
                 : ( <Grid container spacing={24}>
                        {(assets.items && Object.keys(assets.items).length != 0) ?
                            this.renderCards(assets) :
                            this.renderEmpty()}
                     </Grid>)
                }
            </div>
        )
    }
}


function mapStateToProps(state) {
    const { assets, authentication, addasset, transaction, removeasset } = state;
    const { user } = authentication;
    return {
        user,
        assets,
        addasset,
        removeasset,
        transaction
    };
}

Assets.propTypes = {
  Withdraw: PropTypes.element.isRequired,
  Deposit: PropTypes.element.isRequired,
  Transfer: PropTypes.element.isRequired,
  ExpandHistory: PropTypes.element.isRequired
};

const connectedAssets = connect(mapStateToProps)(Assets);
export default connectedAssets;
