import React from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';

import { history } from './helpers/history';
import { alertActions } from './actions/alerts';
import { PrivateRoute } from './components/PrivateRoute';
import { AssetsPage } from './assets/AssetsPage';
import Login from './UserPages/LoginPage';
import Register from './UserPages/RegisterPage';
import { PeriodPage } from './periodic/PeriodPage';
import MonthlyExpanseReport from './expenses/ExpensePage';
import AnnualExpanseReport from './expenses/ExpenseChart';
import SettingsPage from './settings/SettingsPage';
import IndependencePage from './independence/IndependencePage';
import ContractPage from './contract/Contract';

class App extends React.Component {
    constructor(props) {
        super(props);

        const { dispatch } = this.props;
        history.listen((location, action) => {
            dispatch(alertActions.clear());
        });
    }

    render() {
        const { alert } = this.props;
        return (
            <div>
                {alert.message &&
                    <div className={`alert ${alert.type}`}>{alert.message}</div>
                }
                <Router history={history}>
                    <div>
                        <PrivateRoute exact path="/" component={MonthlyExpanseReport} />
                        <PrivateRoute path="/assets" component={AssetsPage} />
                        <PrivateRoute path="/periodic_transactions" component={PeriodPage} />
                        <PrivateRoute path="/report_annual_expanses" component={AnnualExpanseReport} />
                        <PrivateRoute path="/independence" component={IndependencePage} />
                        <PrivateRoute path="/contract" component={ContractPage} />
                        <PrivateRoute path="/settings" component={SettingsPage} />
                        <Route path="/login" component={Login} />
                        <Route path="/register" component={Register} />
                    </div>
                </Router>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App };
