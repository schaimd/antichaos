import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Grid from "@material-ui/core/Grid";
import Hidden from '@material-ui/core/Hidden';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
import { expenseActions } from '../actions/expense';
import { settingsActions } from "../actions/settings";
import getUnixTime from 'date-fns/getUnixTime';
import fromUnixTime from 'date-fns/fromUnixTime';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  DatePicker,
} from '@material-ui/pickers';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import {
  CircularProgressbarWithChildren,
  buildStyles
} from "react-circular-progressbar";
import 'react-circular-progressbar/dist/styles.css';
import { PieChart } from "@opd/g2plot-react";

const styles = theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
    marginTop: theme.spacing.unit * 2,
  },
  table: {
    minWidth: 700,
  },
  header: {
    width: "fit-content",
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: theme.shape.borderRadius,
    backgroundColor: theme.palette.info.light,
    color: "white",
    "& svg": {
     margin: theme.spacing(1.5)
    },
    "& hr": {
     margin: theme.spacing(0, 0.5)
    }
  }
});

class SimpleTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: getUnixTime(new Date()),
        }
        this.render_choose_date = this.render_choose_date.bind(this);
    }

    componentDidMount() {
        this.props.dispatch(expenseActions.getExpenses(this.state.date));
        this.props.dispatch(settingsActions.get_budget());
    }

    componentDidUpdate(PrevProps, PrevStates) {
        console.log('start update');
        if (PrevStates.date != this.state.date) {
            console.log('prevprops date: ' + PrevProps.date );
            console.log('current date: ' + this.state.date );
            this.props.dispatch(expenseActions.getExpenses(this.state.date));
        }
        console.log('after');
        const { addexpense: prev_add } = PrevProps;
        const { addexpense: next_add } =  this.props;
        if (prev_add.adding && next_add.added) {
            this.props.dispatch(expenseActions.getExpenses(this.state.date));
        }

        const { removeexpense: prev_remove } = PrevProps;
        const { removeexpense: next_remove } =  this.props;
        if (prev_remove.removing && next_remove.removed) {
            this.props.dispatch(expenseActions.getExpenses(this.state.date));
        }
    }

    render_choose_date(total, budget) {
        console.log('total is:' + total)
        console.log('budget is: '+ budget)
        const percent = budget == 0 ?  100 : (100 * (total / budget));
        console.log('percent is: ' +  percent)
        const total_value = total == 0 ? "0" : Math.ceil(total).toLocaleString();
        return (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <div style={{ width: "75%" }}>
        		<CircularProgressbarWithChildren
                text={total_value}
                value={percent}
                styles={percent > 100 ? buildStyles({
                    pathColor: "#f00",
                }) : {}}>
                <DatePicker
                    format="MMM, yy"
        		    style={{ width: 56, marginTop: '40%' }}
                    views = {["month", "year"]}
                    margin="normal"
                    id="date-picker-inline"
                    value={fromUnixTime(this.state.date)}
                    onChange={e => this.setState({ date : getUnixTime(e)})}
                    InputProps={{
                		disableUnderline: true
    		              }}
                />
        		</CircularProgressbarWithChildren>
                </div>
              </MuiPickersUtilsProvider>
        );
    }

    render_mobile(classes, expenses) {
        return (
            <div>
            {expenses.items && expenses.items.map(n => {
             var t = fromUnixTime(n.date);
             var f = t.toLocaleDateString();
             return (
                 <ExpansionPanel>
                   <ExpansionPanelSummary
                     expandIcon={<ExpandMoreIcon />}
                     aria-controls={"panel" + n.id + "a-content"}
                     id={"panel" + n.id + "a-header"}
                   >
                     <Typography className={classes.heading}>
                        {n.name + "    " + n.amount.toLocaleString()}
                     </Typography>
                   </ExpansionPanelSummary>
                   <ExpansionPanelDetails>
                     <Grid container spacing={2}>
                        <Grid item>
                        { "Category: " + n.category }
                        </Grid>
                        <Grid item>
                        { "Date: " + f}
                        </Grid>
                     </Grid>
                   </ExpansionPanelDetails>
                   <ExpansionPanelActions>
                       <Fab
                           color="secondary"
                           size="small"
                           aria-label="Remove Expense"
                           onClick={_ => this.props.dispatch(
                                         expenseActions.RemoveExpense(
                                             n.name,
                                             n.amount,
                                             n.category,
                                             n.date))}>
                         <Icon> delete_icon </Icon>
                       </Fab>
                   </ExpansionPanelActions>
                 </ExpansionPanel>
              );
            })}
            </div>
        );
    }

    render_desktop(classes, expenses) {
        return (
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell align="right">Amount</TableCell>
                  <TableCell align="right">Category</TableCell>
                  <TableCell align="right">Date</TableCell>
                  <TableCell align="right">Delete</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {expenses.items && expenses.items.map(n => {
                 var t = fromUnixTime(n.date);
                 var f = t.toLocaleDateString();
                 return (
                    <TableRow>
                      <TableCell>{n.name}</TableCell>
                      <TableCell align="right">{n.amount.toLocaleString()}</TableCell>
                      <TableCell align="right">{n.category}</TableCell>
                      <TableCell align="right">{f}</TableCell>
                      <TableCell align="right">
                          <Fab
                              color="secondary"
                              size="small"
                              aria-label="Remove Expense"
                              onClick={_ => this.props.dispatch(
                                            expenseActions.RemoveExpense(
                                                n.name,
                                                n.amount,
                                                n.category,
                                                n.date))}>
                            <Icon> delete_icon </Icon>
                          </Fab>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
        );
    }

    render_expenses_pie(expenses) {
        var groupBy = (input, key, reducer) => {
            return input.reduce(
                (ret, item) => {
                    ret[item[key]] = (ret[item[key]] || 0) + item[reducer];
                    return ret;
                    },
                {});
        };
        console.log('entering with expense:');
        console.log(expenses);
        let reduced = groupBy(expenses.items, 'category', 'amount');
        console.log('reduced:');
        console.log(reduced);
        let data = [];
        for (let [category, amount] of Object.entries(reduced)) {
            console.log('pushing to data');
            data.push({type: category, value: amount});
        }
        console.log('computed data:');
        console.log(data);
        let pie_config = {
              forceFit: true,
              // title: {
              //   visible: false,
              //   text: 'Expenses Chart',
              // },
              // description: {
              //   visible: false,
              //   text:
              //     'This is black magic',
              // },
              radius: 0.8,
              data: data,
              angleField: 'value',
              colorField: 'type',
              label: {
                visible: true,
                type: 'inner',
              },
          };
        console.log('something is off');
        return (
            <PieChart {...pie_config} />
        );
    }


    render() {
        const { classes, expenses, get_budget } = this.props;
        const total = expenses.loading ?  0 : expenses.total;
        const budget = get_budget.budget && get_budget.budget.budget ? get_budget.budget.budget : 0;
        return (
        <div>
          <Grid container alignItems="center" justify="center" >
            <Grid item>
           { this.render_choose_date(total, budget) }
           </Grid>
           { expenses.items && (<Grid item>
               {this.render_expenses_pie(expenses)}
                </Grid>)
            }
          </Grid>
          <Paper className={classes.root}>
              <Hidden xsDown implementation="css">
                { this.render_desktop(classes, expenses) }
              </Hidden>
              <Hidden smUp implementation="css">
                { this.render_mobile(classes, expenses) }
              </Hidden>
          </Paper>
          </div>
        );
    }
}


SimpleTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    const {  authentication, addexpense, removeexpense, get_budget, expenses } = state;
    return {
        addexpense,
        removeexpense,
        expenses,
        get_budget,
    };
}

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps)(SimpleTable));
